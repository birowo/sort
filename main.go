package main

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/birowo/sort/lib"
)

func generateRandoms(slice []int) {
	rand.Seed(time.Now().UnixNano())
	for i := len(slice) - 1; i > -1; i-- {
		slice[i] = rand.Intn(999) - rand.Intn(999)
	}
}
func main() {
	slice := make([]int, 63)
	generateRandoms(slice)
	fmt.Println("before sort:", slice)
	less := func(i, j int) bool {
		return slice[i] < slice[j]
	}
	swap := func(i, j int) {
		slice[i], slice[j] = slice[j], slice[i]
	}
	sort.Quick(len(slice), less, swap)
	fmt.Println("after sort:", slice)
	for i, item := range slice[1:] {
		if slice[i] > item {
			fmt.Println("error", slice[i], item)
			return
		}
	}
	fmt.Println("ok")
}
