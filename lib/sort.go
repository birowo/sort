package sort

func Quick(slcLen int, less func(int, int) bool, swap func(int, int)) {
	type Part struct {
		start, end int
	}
	parts := make([]Part, slcLen)
	parts[0] = Part{0, slcLen - 1}
	i, j := 0, 1
	for i != j {
		partsIJ := parts[i:j]
		i = j
		for _, part := range partsIJ {
			pivot := part.start
			for i := pivot; i < part.end; i++ {
				if less(i, part.end) {
					swap(i, pivot)
					pivot++
				}
			}
			swap(pivot, part.end)
			if pivot-1 > part.start {
				parts[j] = Part{part.start, pivot - 1}
				j++
			}
			if pivot+1 < part.end {
				parts[j] = Part{pivot + 1, part.end}
				j++
			}
		}
	}
}