# sort

#GOLANG quick sort

**install:** `go get -u gitlab.com/birowo/sort`

**reference:** [iterative implementation of quicksort](https://www.techiedelight.com/iterative-implementation-of-quicksort/)